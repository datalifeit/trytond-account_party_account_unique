datalife_account_party_account_unique
=====================================

The account_party_account_unique module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_party_account_unique/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_party_account_unique)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['Party']


class Party:
    __name__ = 'party.party'
    __metaclass__ = PoolMeta

    creditor = fields.Boolean('Creditor',
        states={
            'readonly': ~Eval('active'),
            'invisible': Eval('creditor_invisible', False)},
        depends=['active', 'creditor_invisible'])
    creditor_invisible = fields.Function(
        fields.Boolean('Creditor invisible'), 'get_creditor_invisible')

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls._error_messages.update({
            'template_account':
                'Cannot find a child account of "%s" in order '
                'to create party account',
            'unique_account_setted':
                'Party "%s" already has a custom account "%s".'
        })
        cls._buttons.update({
            'create_payable_account': {'icon': 'tryton-list-add'},
            'create_receivable_account': {'icon': 'tryton-list-add'}
        })

    @staticmethod
    def default_creditor():
        return False

    @classmethod
    def get_creditor_invisible(cls, records, name=None):
        pool = Pool()
        Conf = pool.get('account.configuration')

        conf = Conf(1)
        _invisible = (conf.account_view_payable_creditor ==
            conf.account_view_payable)
        return {r.id: _invisible for r in records}

    @classmethod
    def create_payable_account(cls, records):
        pool = Pool()
        Conf = pool.get('account.configuration')

        conf = Conf(1)
        for record in records:
            if (record.account_payable and
                    conf.default_account_payable != record.account_payable):
                cls.raise_user_warning('unique_account_setted_%s' % record.id,
                    'unique_account_setted',
                    (record.rec_name, record.account_payable.rec_name))
            _fieldname = 'payable'
            if record.creditor:
                _fieldname = 'payable_creditor'
            record.account_payable = cls._create_unique_account(
                _fieldname, record.name)
        cls.save(records)

    @classmethod
    def create_receivable_account(cls, records):
        pool = Pool()
        Conf = pool.get('account.configuration')

        conf = Conf(1)
        for record in records:
            if (record.account_receivable and
                    conf.default_account_receivable !=
                    record.account_receivable):
                cls.raise_user_warning('unique_account_setted_%s' % record.id,
                    'unique_account_setted',
                    (record.rec_name, record.account_receivable.rec_name))
            record.account_receivable = cls._create_unique_account(
                'receivable', record.name)
        cls.save(records)

    @classmethod
    def _create_unique_account(cls, type_, name):
        pool = Pool()
        Sequence = Pool().get('ir.sequence')
        Conf = pool.get('account.configuration')
        Account = pool.get('account.account')

        conf = Conf(1)
        _account = getattr(conf, 'account_view_%s' % type_, None)

        if not _account:
            return None
        template = Account.search([
            ('parent', '=', _account.id)], limit=1,
            order=[('code', 'ASC')])
        if not template:
            cls.raise_user_error('template_account', _account.rec_name)
        template, = template
        _code = Sequence.get_id(
            getattr(conf, 'account_view_%s_sequence' % type_, None).id)
        _code = template.code[:len(template.code) - len(_code)] + _code

        with Transaction().set_context(_check_access=False):
            new_account, = Account.copy([template], default={
                'code': _code,
                'name': name
            })
        return new_account

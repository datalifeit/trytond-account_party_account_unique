# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .party import Party
from .configuration import Configuration, ConfigurationAccount


def register():
    Pool.register(
        Configuration,
        Party,
        ConfigurationAccount,
        module='account_party_account_unique', type_='model')

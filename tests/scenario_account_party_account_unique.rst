=====================================
Account Party Account Unique Scenario
=====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax

Install account_party_account_unique::

    >>> config = activate_modules('account_party_account_unique')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.credit_account = cash
    >>> cash_journal.debit_account = cash
    >>> cash_journal.save()

Configure accounts::

    >>> Account = Model.get('account.account')
    >>> payable_view, = Account.duplicate([accounts['payable']], {
    ...     'name': 'Payable', 'kind': 'view', 'type': None, 'party_required':False})
    >>> payable, = Account.duplicate([accounts['payable']], {
    ...     'parent': payable_view.id, 'code': '10000'})
    >>> creditor_view, = Account.duplicate([accounts['payable']], {
    ...     'name': 'Creditor', 'kind': 'view', 'party_required':False})
    >>> creditor, = Account.duplicate([accounts['payable']], {
    ...     'parent': creditor_view.id, 'code': '11000', 'party_required':False})
    >>> receivable_view, = Account.duplicate([accounts['receivable']], {
    ...     'name': 'Receivable', 'kind': 'view', 'party_required':False})
    >>> receivable, = Account.duplicate([accounts['receivable']], {
    ...     'parent': receivable_view.id, 'code': '20000'})
    >>> conf = Model.get('account.configuration')(1)
    >>> conf.default_account_payable = payable
    >>> conf.default_account_receivable = receivable
    >>> conf.account_view_payable = payable_view
    >>> conf.account_view_payable_creditor = creditor_view
    >>> conf.account_view_receivable = receivable_view
    >>> conf.save()

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party 1')
    >>> party.save()
    >>> party.click('create_receivable_account')
    >>> party.reload()
    >>> party.account_receivable != conf.default_account_receivable
    True
    >>> (party.account_receivable.code, party.account_receivable.name)
    (u'20001', u'Party 1')
    >>> party.click('create_payable_account')
    >>> party.reload()
    >>> party.account_payable != conf.default_account_payable
    True
    >>> (party.account_payable.code, party.account_payable.name)
    (u'10001', u'Party 1')
    >>> party2 = Party(name='Party 2')
    >>> party2.creditor = True
    >>> party2.save()
    >>> party2.click('create_payable_account')
    >>> party2.reload()
    >>> (party2.account_payable.code, party2.account_payable.name)
    (u'11001', u'Party 2')
    >>> party2.click('create_payable_account') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserWarning: ...
